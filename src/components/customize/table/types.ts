export interface tableOptions {
    // 类型
    tableType?: 'selection' | 'index' | 'expand'
    // 表头
    label?: string
    // 字段名
    prop?: string
    // 宽度
    width?: string | number
    // 对齐方式
    align?: 'left' | 'center' | 'right'
    // 自定义列插槽名
    slot?: string,
    // 是否开启操作列
    operate?: boolean
    // 是否列编辑
    editable?: boolean
}