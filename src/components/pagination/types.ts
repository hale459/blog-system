export interface paginationTypes {
    total: number
    pageSizes: number[]
    pageSize: number
    current: number
}


