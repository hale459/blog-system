/// <reference types="vite/client" />

declare module '*.vue' {
    import type {DefineComponent} from 'vue'
    const component: DefineComponent<{}, {}, any>
    export default component
}

declare module 'nprogress'
declare module 'js-cookie'
declare module '@kangc/v-md-editor/lib/*'
declare module 'codemirror'
declare module 'highlight.js'
