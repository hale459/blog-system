import {createApp} from 'vue'
import App from './App.vue'
// element plus
import ElementPlus from "./plugins/element-plus";
// axios
import {axios, VueAxios} from './utils/axios'
// router
import router from './router'
// pinia
import {createPinia} from "pinia"
// markdown
import VMdEditor from './plugins/v-md-editor'

const app = createApp(App)

app.use(VueAxios, axios)
    .use(router)
    .use(createPinia())
    .use(ElementPlus)
    .use(VMdEditor)
    .mount('#app')


