import {defineStore} from "pinia";

interface IMainStore {
}

export const useMainStore = defineStore('main', {

    state: (): IMainStore => {
        return {}
    },

    getters: {},

    actions: {}
})

