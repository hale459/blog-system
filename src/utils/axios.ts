import axios, {AxiosRequestConfig, AxiosResponse} from "axios"
import VueAxios from "vue-axios"
import {ElNotification} from "element-plus/es"
import nProgress from 'nprogress'
import 'nprogress/nprogress.css'
import {ElMessage} from "element-plus";

// 初始化axios
axios.create({
    baseURL: 'http://127.0.0.1:8080',  //设置服务器地址
    timeout: 6 * 1000, // 超时设置
    withCredentials: true, // 检查跨站点访问控制
})

// 请求拦截器
axios.interceptors.request.use(
    (config: AxiosRequestConfig) => {
        nProgress.start()
        // 添加请求信息 token
        return config;
    }, ((error: any) => {
        return Promise.reject(error);
    }))

// 响应拦截器
axios.interceptors.response.use(
    (response: AxiosResponse) => {
        nProgress.done()
        return response
    },
    ((error: any) => {
        console.log(error);
        ElNotification.error('发生错误！')
        return Promise.reject(error);
    }))

export {axios, VueAxios}
