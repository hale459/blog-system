import {axios} from "../../utils/axios";
import {loginVo} from "./types";

// 获取验证码
export const getVerify = () => {
    return axios({
        method: 'GET',
        url: '/api//getVerify'
    })
}

// 校验验证码
export const checkVerify = (code: string) => {
    return axios({
        method: 'GET',
        url: '/api/checkVerify',
        params: {
            code: code
        }
    })
}
// 登录校验
export const login = (loginInfo: loginVo) => {
    return axios({
        method: 'POST',
        url: '/api/login',
        data: loginInfo

    })
}
