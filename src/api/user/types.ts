export interface loginVo {
    username: string
    password: string
    code: string
    rememberMe: boolean
}