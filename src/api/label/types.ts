// 表格数据
export interface Label {
    id?: number
    name: string
    alias: string
    description: string
    createTime?: string
    updateTime?: string
}

// 分页查询数据
export interface queryData {
    currentPage: number
    pageSize: number
    searchVal?: string
    searchDate?: string
}

// 页面数据
export interface pageData {
    showSearchPanel: boolean// 是否显示搜索面板
    insertDialogVisible: boolean// 新增对话框状态
    editorDialogVisible: boolean// 编辑对话框状态
    total: number// 数据总条数
    loading: boolean// 加载状态
    labelIds: number[]// 批量删除ID数组
    tableData: Label[]// 表格数据
}