import {axios} from '../../utils/axios'
import {queryData, Label} from "./types";

// 获取所有标签
export const getLabel = () => {
    return axios({
        method: 'GET',
        url: '/api/getLabel'
    })
}

// 分页查询标签信息
export const getLabelByPage = (data: queryData) => {
    return axios({
        method: 'GET',
        url: '/api/getLabelByPage',
        params: data
    })
}

// 新增标签
export const addLabel = (data: Label) => {
    return axios({
        method: 'POST',
        url: '/api/addLabel',
        data
    })
}

// 删除标签
export const deleteLabel = (id: number) => {
    return axios({
        method: 'DELETE',
        url: '/api/deleteLabel/' + id,
    })
}

// 批量删除标签
export const deleteBatchLabel = (data: number[]) => {
    return axios({
        method: 'DELETE',
        url: '/api/deleteBatchLabel',
        data
    })
}

// 修改标签
export const updateLabel = (data: Label) => {
    return axios({
        method: 'PUT',
        url: '/api/updateLabel',
        data
    })
}