import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'
import Home from '../views/Home.vue'
import nProgress from 'nprogress'
import 'nprogress/nprogress.css'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/login',
        component: () => import('../views/Login.vue'),
    },
    {
        path: '/404',
        component: () => import('../views/error/404.vue')
    },
    {
        path: '/401',
        component: () => import('../views/error/401.vue')
    },
    // 一层菜单
    {
        path: '/',
        component: Home,
        redirect: '/dashboard',
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                meta: {
                    title: '首页'
                },
                component: () => import('../views/dashboard/Dashboard.vue')
            }
        ]
    },
    // 二层菜单
    {
        path: '/article',
        component: Home,
        meta: {
            title: '文章管理'
        },
        children: [
            {
                path: 'write',
                name: 'article-write',
                meta: {
                    title: '写文章'
                },
                component: () => import('../views/article/ArticleWrite.vue')
            },
            {
                path: 'list',
                name: 'article-list',
                meta: {
                    title: '文章列表'
                },
                component: () => import('../views/article/ArticleList.vue')
            },
            {
                path: 'label',
                name: 'article-label',
                meta: {
                    title: '标签管理'
                },
                component: () => import('../views/article/ArticleLabel.vue')
            }
        ]
    },
    // 三层菜单
    {
        path: '/system',
        component: Home,
        meta: {
            title: '系统管理'
        },
        children: [
            {
                path: 'user',
                name: 'user',
                meta: {
                    title: '用户管理'
                },
                component: () => import('../views/system/User.vue')
            },
            {
                path: 'role',
                name: 'role',
                meta: {
                    title: '角色管理'
                },
                component: () => import('../views/system/Role.vue')
            },
            {
                path: 'menu',
                name: 'menu',
                meta: {
                    title: '菜单管理'
                },
                component: () => import('../views/system/Menu.vue')
            }
        ]
    },
    // 四层菜单
    {
        path: '/customize',
        component: Home,
        meta: {
            title: '组件管理'
        },
        children: [
            {
                path: 'pagination',
                name: 'pagination',
                meta: {
                    title: '分页插件'
                },
                component: () => import('../views/customize/Pagination.vue')
            },
            {
                path: 'table',
                name: 'table',
                meta: {
                    title: '表格插件'
                },
                component: () => import('../views/customize/Table.vue')
            }
        ]
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

router.beforeEach((to, from, next) => {
    nProgress.start()
    next()
})

router.afterEach(() => {
    nProgress.done()
})

//  NProgress配置
nProgress.configure({
    ease: 'linear',
    speed: 500,
    showSpinner: false  // 是否使用进度环
})

export default router