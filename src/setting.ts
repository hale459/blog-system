export interface ISettings {
    title: string// 网站标题
}

export const settings: ISettings = {
    title: '博客后台'
}

/****************************************************************************/

export interface menuItemTypes {
    name: string// 菜单名称
    icon?: string// 菜单图标
    path?: string// 菜单路径
    children?: menuItemTypes[]// 子菜单
}

// 菜单数据
export const menuList: Array<menuItemTypes> = [
    {
        name: '首页',
        path: '/dashboard',
        icon: 'location'
    },
    {
        name: '文章管理',
        path: '/article',
        icon: 'setting',
        children: [
            {
                name: '写文章',
                path: '/article/write',
                icon: 'document'
            },
            {
                name: '文章列表',
                path: '/article/list',
                icon: 'document'
            },
            {
                name: '标签管理',
                path: '/article/label',
                icon: 'document'
            }
        ]
    },
    {
        name: '系统管理',
        path: '/system',
        icon: 'setting',
        children: [
            {
                name: '用户管理',
                path: '/system/user',
                icon: 'document'
            },
            {
                name: '角色管理',
                path: '/system/role',
                icon: 'document'
            },
            {
                name: '菜单管理',
                path: '/system/menu',
                icon: 'document'
            }
        ]
    },
    {
        name: '组件管理',
        path: '/customize',
        icon: 'setting',
        children: [
            {
                name: '分页组件',
                path: '/customize/pagination',
                icon: 'document'
            },
            {
                name: '表格组件',
                path: '/customize/table',
                icon: 'document'
            }
        ]
    }
]